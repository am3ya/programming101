package programming_101.java.data_structures;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

class PriorityQueueExample {
  public static void main(String[] args) {
    System.out.println("Priority Queue Example");
    // PriorityQueue<Integer> small = new PriorityQueue<>(Collections.reverseOrder());
    Queue<Integer> queue = new PriorityQueue<>();
    fillQueue(queue);
    System.out.println("Sorted in increasing order by default\n=> " + queue);
    printQueue(queue);

    queue = new PriorityQueue<>(Collections.reverseOrder());
    fillQueue(queue);
    System.out.println("\nOverride comparator to sort in decreasing order\n=> " + queue);    
    printQueue(queue);
  }

  private static void fillQueue(Queue<Integer> queue) {
    queue.add(5);
    queue.add(2);
    queue.add(1);
    queue.add(4);
    queue.add(3);    
  }

  private static void printQueue(Queue<Integer> queue) {
    while(!queue.isEmpty()) {
      System.out.print(" => " + queue.poll());
    }
    System.out.println("");
  }
}