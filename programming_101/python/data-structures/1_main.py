# Source: https://www.journaldev.com/17752/python-main-function
# When a python program is executed, python interpreter starts executing code inside it. 
# It also sets few implicit variable values, one of them is __name__ whose value is set as __main__.

# If the python source file is imported as module, 
#   python interpreter sets the __name__ value to module name, 
#   so the if condition will return false and main method will not be executed.

print("Hello")

print("__name__ value: ", __name__)

def main():
    print("python main function")

# For python main function, we have to define a function and then use if __name__ == '__main__' condition to execute this function.
if __name__ == '__main__':
    main()