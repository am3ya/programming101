from queue import PriorityQueue

def priority_queue_increasing():
  print("PriorityQueue is sorted in increasing order")
  queue = PriorityQueue()
  queue.put(4)
  queue.put(2)
  queue.put(1)
  queue.put(3)

  while not queue.empty():
    next_item = queue.get()
    print(next_item)

def list_heaq_default_increasing():
  import heapq
  list = []
  heapq.heappush(list, 4)
  heapq.heappush(list, 2)
  heapq.heappush(list, 1)
  heapq.heappush(list, 3)

  while len(list) > 0:
    next_item = heapq.heappop(list)
    print(next_item)

def list_heaq_decreasing():
  import heapq
  list = []
  heapq.heappush(list, (-4, 4))
  heapq.heappush(list, (-2, 2))
  heapq.heappush(list, (-1, 1))
  heapq.heappush(list, (-3, 3))

  while len(list) > 0:
    (_, next_item) = heapq.heappop(list)
    print(next_item)

if __name__ == '__main__':
  # priority_queue_increasing()
  # list_heaq_default_increasing()
  list_heaq_decreasing()